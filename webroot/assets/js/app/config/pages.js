define([
        "controllers/pages/loading",
        "controllers/pages/home",
        "controllers/pages/login",
        "controllers/pages/reg",
        "controllers/pages/modules",
        "controllers/pages/program",
        "controllers/pages/guide1",
        "controllers/pages/guide2",
        "controllers/pages/guide3",
        "controllers/pages/test",
        "controllers/pages/test1",
        "controllers/pages/test2",
        "controllers/pages/test3",
        "controllers/pages/analyse1",
        "controllers/pages/analyse2",
        "controllers/pages/analyse3",
        "controllers/pages/prize",
        "controllers/pages/check",
        "controllers/pages/searchshop",
        "controllers/pages/searcharea",
        "controllers/pages/listshop",
        "controllers/pages/listarea",
        "controllers/pages/listall",
    ],
    function(Loading, Home, Login, Reg, Modules, Program, Guide1, Guide2, Guide3, Test, Test1, Test2, Test3, Analyse1, Analyse2, Analyse3, Prize, Check, Searchshop, Searcharea, Listshop, Listarea, Listall) {
        var pages = [
            {
                routeId: 'loading',
                type: 'main',
                landing: true,
                page: function() {
                    return new Loading.View({ template: "pages/loading" });
                }
            },
            {
                routeId: 'home',
                type: 'main',
                landing: false,
                page: function() {
                    return new Home.View({ template: "pages/home" });
                }
            },
            {
                routeId: 'login',
                type: 'main',
                landing: false,
                page: function() {
                    return new Login.View({ template: "pages/login" });
                }
            },
            {
                routeId: 'reg',
                type: 'main',
                landing: false,
                page: function() {
                    return new Reg.View({ template: "pages/reg" });
                }
            },
            {
                routeId: 'modules',
                type: 'main',
                landing: false,
                page: function() {
                    return new Modules.View({ template: "pages/modules" });
                }
            },
            {
                routeId: 'program',
                type: 'main',
                landing: false,
                page: function() {
                    return new Program.View({ template: "pages/program" });
                }
            },
            {
                routeId: 'guide1',
                type: 'main',
                landing: false,
                page: function() {
                    return new Guide1.View({ template: "pages/guide1" });
                }
            },
            {
                routeId: 'guide2',
                type: 'main',
                landing: false,
                page: function() {
                    return new Guide2.View({ template: "pages/guide2" });
                }
            },
            {
                routeId: 'guide3',
                type: 'main',
                landing: false,
                page: function() {
                    return new Guide3.View({ template: "pages/guide3" });
                }
            },
            {
                routeId: 'test',
                type: 'main',
                landing: false,
                page: function() {
                    return new Test.View({ template: "pages/test" });
                }
            },
            {
                routeId: 'test1',
                type: 'main',
                landing: false,
                page: function() {
                    return new Test1.View({ template: "pages/test1" });
                }
            },
            {
                routeId: 'test2',
                type: 'main',
                landing: false,
                page: function() {
                    return new Test2.View({ template: "pages/test2" });
                }
            },
            {
                routeId: 'test3',
                type: 'main',
                landing: false,
                page: function() {
                    return new Test3.View({ template: "pages/test3" });
                }
            },
            {
                routeId: 'analyse1',
                type: 'main',
                landing: false,
                page: function() {
                    return new Analyse1.View({ template: "pages/analyse1" });
                }
            },
            {
                routeId: 'analyse2',
                type: 'main',
                landing: false,
                page: function() {
                    return new Analyse2.View({ template: "pages/analyse2" });
                }
            },
            {
                routeId: 'analyse3',
                type: 'main',
                landing: false,
                page: function() {
                    return new Analyse3.View({ template: "pages/analyse3" });
                }
            },
            {
                routeId: 'prize',
                type: 'main',
                landing: false,
                page: function() {
                    return new Prize.View({ template: "pages/prize" });
                }
            },
            {
                routeId: 'check',
                type: 'main',
                landing: false,
                page: function() {
                    return new Check.View({ template: "pages/check" });
                }
            },
            {
                routeId: 'searchshop',
                type: 'main',
                landing: false,
                page: function() {
                    return new Searchshop.View({ template: "pages/searchshop" });
                }
            },
            {
                routeId: 'searcharea',
                type: 'main',
                landing: false,
                page: function() {
                    return new Searcharea.View({ template: "pages/searcharea" });
                }
            },
            {
                routeId: 'listshop',
                type: 'main',
                landing: false,
                page: function() {
                    return new Listshop.View({ template: "pages/listshop" });
                }
            },
            {
                routeId: 'listarea',
                type: 'main',
                landing: false,
                page: function() {
                    return new Listarea.View({ template: "pages/listarea" });
                }
            },
            {
                routeId: 'listall',
                type: 'main',
                landing: false,
                page: function() {
                    return new Listall.View({ template: "pages/listall" });
                }
            },
        ];
        return pages;
    });