// Layout module
define(["app", "controllers/base/layout"],

    function(app, BaseLayout) {

        var Layout = {};

        Layout.View = BaseLayout.View.extend({
            el: "#main",
            template: "layouts/main",
            piconTween: null,
            liconTween: null,

            beforeRender: function() {

                var done = this.async();
                done();

            },

            afterRender: function() {
                // weChat弹出输入法收不回 fix
                var u = navigator.userAgent;
                var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
                var isFocus = 0; // 默认没有在输入

                // ios wechat
                if(u.toLowerCase().match(/MicroMessenger/i) == 'micromessenger') {
                    if(isiOS) {
                        $(document).on('focusin', function() {  
                            isFocus = 1;
                        });
                        $(document).on('focusout', function() {
                            isFocus = 0;
                            if($('body')[0].scrollTop > 0) {
                                // iphone 6 
                                setTimeout(function() {
                                    if(isFocus == 0) {
                                        // console.log('执行收起');
                                        $('body').scrollTop($('body')[0].scrollHeight);
                                        $('html').scrollLeft(0);
                                    } else {
                                        // 
                                    }
                                }, 300);
                            } else {
                                // iphone x
                                setTimeout(function() {
                                    // console.log('执行收起');
                                    //alert($('body')[0].scrollHeight);
                                    $('body').scrollTop($('body')[0].scrollHeight);
                                    // 针对iphoneX的更新
                                    $('html').scrollLeft(0);
                                }, 300);
                            };
                        });
                    };
                };

                // this.piconTween = TweenMax.fromTo(this.$(".rotate-portrait .icon"), 0.8, { rotation: 90, transformOrigin: "50% 50%" }, { rotation: 0, transformOrigin: "50% 50%", repeat: -1, repeatDelay: 0.8 });
                // this.liconTween = TweenMax.fromTo(this.$(".rotate-landscape .icon"), 0.8, { rotation: 0, transformOrigin: "50% 50%" }, { rotation: 90, transformOrigin: "50% 50%", repeat: -1, repeatDelay: 0.8 });

                // this.piconTween.pause();
                // this.liconTween.pause();
                // var opx = "";
                // opx = "music";
                // $("#opAudio").remove();
                // $("body").append('<audio id="opAudio" src="assets/audio/' + opx + '.mp3" autoplay="" loop="false"></audio>');

                // if (window.WeixinJSBridge) {
                //     WeixinJSBridge.invoke('getNetworkType', {}, function(e) {

                //         $("#opAudio").get(0).play();
                //     }, false);
                // } else {
                //     document.addEventListener("WeixinJSBridgeReady", function() {
                //         WeixinJSBridge.invoke('getNetworkType', {}, function(e) {

                //             $("#opAudio").get(0).play();
                //         });
                //     }, false);
                // }
                // $(".musicKai").show();
                // $(".music").hide();

                // $(".musicKai").click(function() {
                //     $("#opAudio").remove();

                //     $(".musicKai").hide();
                //     $(".music").show();
                // });
                // $(".music").click(function() {
                //     $(".musicKai").show();
                //     $(".music").hide();
                //     $("#opAudio").remove();
                //     $("body").append('<audio id="opAudio" src="assets/audio/' + opx + '.mp3" autoplay="" loop="false"></audio>');

                //     if (window.WeixinJSBridge) {
                //         WeixinJSBridge.invoke('getNetworkType', {}, function(e) {

                //             $("#opAudio").get(0).play();
                //         }, false);
                //     } else {
                //         document.addEventListener("WeixinJSBridgeReady", function() {
                //             WeixinJSBridge.invoke('getNetworkType', {}, function(e) {

                //                 $("#opAudio").get(0).play();
                //             });
                //         }, false);
                //     }

                //     $("#opAudio").get(0).play();
                // })
                // var tmusic1 = new TimelineMax({ yoyo: false, repeat: -1 });

                // tmusic1.to($(".music"), 2, { transformOrigin: "50% 50%", rotation: 360, yoyo: true, ease: Power0.easeNone }, '+=0.1')
                // var tmusic2 = new TimelineMax({ yoyo: false, repeat: -1 });

                // tmusic2.to($(".musicKai"), 2, { transformOrigin: "50% 50%", rotation: 360, yoyo: true, ease: Power0.easeNone }, '+=0.1');
            },

            resize: function(ww, wh, orient) {

                if (app.router.pageHolders.length > 0) {

                    if (orient == "landscape" && app.router.pageHolders[app.router.pageHolders.length - 1].defaultOrientation == "portrait") {
                        this.$(".rotate-portrait").show();
                        this.$(".rotate-landscape").hide();

                        // this.piconTween.play();
                        // this.liconTween.pause();

                    } else if (orient == "portrait" && app.router.pageHolders[app.router.pageHolders.length - 1].defaultOrientation == "landscape") {
                        this.$(".rotate-landscape").show();
                        this.$(".rotate-portrait").hide();

                        // this.liconTween.play();
                        // this.piconTween.pause();

                    } else {
                        this.$(".rotate-portrait").hide();
                        this.$(".rotate-landscape").hide();

                        // this.piconTween.pause();
                        // this.liconTween.pause();
                    }
                }
            }
        });

        // Return the module for AMD compliance.
        return Layout;

    });
