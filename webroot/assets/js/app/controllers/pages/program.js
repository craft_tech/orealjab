// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require([],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'program');
            };

            // score
            switch(JSON.parse(localStorage.getItem('studyStatus'))) {
                case 0:
                    // 还没开始
                    break;
                case 1:
                    // 过了level1
                    $('.level1 .score').text(JSON.parse(localStorage.getItem('level1score')));
                    if(JSON.parse(localStorage.getItem('level2score')) >= 3.5) {
                        // 过了level2
                        $('.level2 .score').text(JSON.parse(localStorage.getItem('level2score')));
                    };
                    break;
                case 2:
                    $('.level1 .score').text(JSON.parse(localStorage.getItem('level1score')));
                    if(JSON.parse(localStorage.getItem('level1score')) >= 80) {
                        // 过了level1
                        if(JSON.parse(localStorage.getItem('level2score')) < 3.5) {
                            // 没过level2
                            $('.level2 .score').text(JSON.parse(localStorage.getItem('level2score')));
                        } else {
                            // 过了level2
                            $('.level2 .score').text(JSON.parse(localStorage.getItem('level2score')));
                            $('.level3 .score').text(JSON.parse(localStorage.getItem('level3score')));
                        }
                    };
                    break;
                case 3:
                    // 全过
                    $('.level1 .score').text(JSON.parse(localStorage.getItem('level1score')));
                    $('.level2 .score').text(JSON.parse(localStorage.getItem('level2score')));
                    $('.level3 .score').text(JSON.parse(localStorage.getItem('level3score')));
                    break;
                default:
                    //
            };

            // test
            if(JSON.parse(localStorage.getItem('level1score')) >= 80) {
                // // 过了level1
                $('.level1').addClass('pass');
                $('.level2').removeClass('lock');
                $('.level2').addClass('unlock');
                $('.level1 .status img').attr('src', 'assets/images/program/pass.png');
                if(JSON.parse(localStorage.getItem('level2score')) >= 3.5) {
                    // 过了level2
                    $('.level2').addClass('pass');
                    $('.level3').removeClass('lock');
                    $('.level3').addClass('unlock');
                    $('.level2 .status img').attr('src', 'assets/images/program/pass.png');
                    if(JSON.parse(localStorage.getItem('level3score')) == 100) {
                        // 过了level3
                        $('.level3').addClass('pass');
                        $('.level3 .status img').attr('src', 'assets/images/program/pass.png');
                    };
                };
            };

            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.program_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // unlock
            $('.level1.unlock .btn-study').on('click', function() {
                tl.kill();
                app.router.goto('guide1', ['program']);
            });

            $('.level2.unlock .btn-study').on('click', function() {
                tl.kill();
                app.router.goto('guide2', ['program']);
            });

            $('.level3.unlock .btn-study').on('click', function() {
                tl.kill();
                app.router.goto('guide3', ['program']);
            });

            // back modules
            $('.btn-modules').on('click', function() {
                tl.kill();
                app.router.goto('modules');
            });
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
