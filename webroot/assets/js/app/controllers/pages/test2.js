// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/fx", "vendor/zepto/fx_methods"],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'test2');
            };

            if(JSON.parse(localStorage.getItem('level2score')) == 0) {
                // 要答所有题目
                $('.qa-item').addClass('show');
                $('.qa-title').addClass('show');
            } else {
                // 调取错过的题目
                $('.modal-loading').fadeIn('fast');
                $.ajax({
                    type: 'post',
                    url: 'https://campaign.lorealparis.com.cn/jablearning/server/GetWrongQusetions',
                    dataType: 'json',
                    // contentType: 'application/json',
                    data: {
                        name: localStorage.getItem('userName'),
                        storeNum: localStorage.getItem('userNum'),
                        level: 2
                    },
                    // async: false,
                    cache: false,
                    success: function(data) {
                        console.log(data);
                        if(data.msg != '') {
                            if(/未登录+/.test(data.msg)) {
                                localStorage.removeItem('userName');
                                localStorage.removeItem('userNum');
                                tl.kill();
                                app.router.goto('login');
                                return;
                            };
                        };
                        if(data.status) {
                            var arr = data.WrongQuestion.split(','),
                                len = arr.length;
                            if(len > 0) {
                                for(var i = 0; i < len; i++) {
                                    $('.qa-item' + arr[i]).addClass('show');
                                    if(arr[i] <= 8) {
                                        // 属于第一组
                                        $('.qa-title1').addClass('show');
                                    } else if(arr[i] > 8 && arr[i] <= 16) {
                                        // 属于第二组
                                        $('.qa-title2').addClass('show');
                                    } else if(arr[i] > 16 && arr[i] <= 21) {
                                        // 属于第三组
                                        $('.qa-title3').addClass('show');
                                    } else {
                                        // 属于第四组
                                        $('.qa-title4').addClass('show');
                                    };
                                };
                            };
                        };
                    },
                    error: function() {
                        //
                        alert('网络错误，请刷新重试');
                    },
                    complete: function(data) {
                        // 调取完毕
                        $('.modal-loading').fadeOut('fast');
                    }
                });
            };

            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.test2_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // 答题进度条
            $('.ran').bind('input', function() {
                $(this).css({'background': 'linear-gradient(to right, #f6d8ac ' + (($(this).val() - 1) * 25) + '%, white ' + (($(this).val() - 1) * 25) + '%)'});
            });

            // 分数点击 
            $('.score div').on('click', function() {
                $(this).parent('.score').prev().val(($(this).index() + 1));
                $($(this).parent('.score').prev()).css({'background': 'linear-gradient(to right, #f6d8ac ' + ($(this).index() * 25) + '%, white ' + ($(this).index() * 25) + '%)'});
            });
            
            // submit
            var canSubmit = 1; // 默认可以提交
            $('.test2_page .btn-submit').off('click');
            $('.test2_page .btn-submit').on('click', function() {
                var len = $('.qa-item.show').length,
                    answerO = {}, // 需要提交的答案
                    wrongAn = []; // 错题组
                for(var i = 0; i < len; i++) {
                    // 显示的题目
                    answerO['an' + $($('.qa-item.show')[i]).data('index')] = $($('.qa-item.show')[i]).find('.ran').val();
                    if($($('.qa-item.show')[i]).find('.ran').val() < 5) {
                        wrongAn.push($($('.qa-item.show')[i]).data('index'));
                    };
                };
                if(wrongAn.length > 0) {
                    answerO['WrongQuestion'] = wrongAn.join(',');
                };
                // console.log(answerO);
                // can submit
                if(canSubmit == 1) {
                    // 调用中不能再次提交
                    canSubmit = 0;
                    $('.modal-loading').fadeIn('fast');
                    $.ajax({
                        type: 'post',
                        url: 'https://campaign.lorealparis.com.cn/jablearning/Server/GetScore',
                        dataType: 'json',
                        // contentType: 'application/json',
                        data: {
                            name: localStorage.getItem('userName'),
                            storeNum: localStorage.getItem('userNum'),
                            level: 2,
                            answer: JSON.stringify(answerO)
                        },
                        // async: false,
                        cache: false,
                        success: function(data) {
                            console.log(data);
                            if(data.msg != '') {
                                // alert(data.msg);
                                if(/未登录+/.test(data.msg)) {
                                    localStorage.removeItem('userName');
                                    localStorage.removeItem('userNum');
                                    tl.kill();
                                    app.router.goto('login');
                                    return;
                                };
                            };
                            if(data.status) {
                                localStorage.setItem('studyStatus', JSON.stringify(data.data.StudyStatus)); // 存储学习状态
                                localStorage.setItem('level2score', JSON.stringify(data.score)); // 更新分数
                                sessionStorage.setItem('errorArr2', JSON.stringify(wrongAn)); // 存储低于五分的
                                if(wrongAn.length > 0) {
                                    // 有低于五分的
                                    sessionStorage.setItem('rightNum2', JSON.stringify(len - wrongAn.length)); // 存储对题数
                                    sessionStorage.setItem('wrongNum2', JSON.stringify(wrongAn.length)); // 存储错题数
                                } else {
                                    sessionStorage.setItem('rightNum2', JSON.stringify(len)); // 存储对题数
                                    sessionStorage.setItem('wrongNum2', JSON.stringify(0)); // 存储错题数
                                };
                                tl.kill();
                                app.router.goto('analyse2');
                            };
                        },
                        error: function() {
                            //
                            alert('提交失败，请重试');
                        },
                        complete: function(data) {
                            //
                            canSubmit = 1;
                            $('.modal-loading').fadeOut('fast');
                        }
                    });
                };
            });

            // back
            $('.btn-back').on('click', function() {
                tl.kill();
                app.router.goto(context.args[0]);
            });
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
