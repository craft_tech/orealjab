// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/zepto.html5Loader.min"],
            function() {
                done();
                sessionStorage.setItem('loading', true);
                // console.log(sessionStorage.getItem('currentPage'));
            });
        },
        afterRender: function() {
            if((localStorage.getItem('userName') != null || localStorage.getItem('userName') != undefined) && (localStorage.getItem('userNum') != null || localStorage.getItem('userNum') != undefined)) {
                // 有用户缓存，不用登录
                // 获取最新进度
                $.ajax({
                    type: 'post',
                    url: 'https://campaign.lorealparis.com.cn/jablearning/Server/GetLearningProgress',
                    dataType: 'json',
                    // contentType: 'application/json',
                    data: {
                        name: localStorage.getItem('userName'),
                        storeNum: localStorage.getItem('userNum')
                    },
                    // async: false,
                    cache: false,
                    success: function(data) {
                        console.log(data);
                        if(data.msg != '') {
                            // alert(data.msg);
                            if(/未登录+/.test(data.msg)) {
                                localStorage.removeItem('userName');
                                localStorage.removeItem('userNum');
                            };
                        };
                        if(data.status) {
                            localStorage.setItem('studyStatus', JSON.stringify(data.data.StudyStatus)); // 存储学习状态
                            localStorage.setItem('level1score', JSON.stringify(data.data.EntryLevelScore)); // 存储level1分数
                            localStorage.setItem('level2score', JSON.stringify(data.data.AdvancedScore)); // 存储level2分数
                            localStorage.setItem('level3score', JSON.stringify(data.data.MatureScore)); // 存储level3分数
                            localStorage.setItem('level1prize', JSON.stringify(data.data.EntryLevelPrize)); // 存储level1奖品
                            localStorage.setItem('level2prize', JSON.stringify(data.data.AdvancedPrize)); // 存储level2奖品
                            localStorage.setItem('level3prize', JSON.stringify(data.data.MaturePrize)); // 存储level3奖品
                            localStorage.setItem('level1prizeEx', JSON.stringify(data.data.EntryLevelExchangePrize)); // 存储level1奖品兑换状态
                            localStorage.setItem('level2prizeEx', JSON.stringify(data.data.AdvancedExchangePrize)); // 存储level2奖品兑换状态
                            localStorage.setItem('level3prizeEx', JSON.stringify(data.data.MatureExchangePrize)); // 存储level3奖品兑换状态
                            // localStorage.setItem('percentA', JSON.stringify(data.data.AmaniRatio)); // 存储阿玛尼平均分
                            // localStorage.setItem('percentB', JSON.stringify(data.data.BiothermRatio)); // 存储碧欧泉平均分
                            // localStorage.setItem('percentL', JSON.stringify(data.data.LancomeRatio)); // 存储兰蔻平均分
                            // localStorage.setItem('percentS', JSON.stringify(data.data.ShuUemuraRatio)); // 存储植村秀平均分
                        };
                    },
                    error: function() {
                        //
                        alert('网络错误，请刷新重试');
                    },
                    complete: function(data) {
                        //
                    }
                }); 
            };

            // wechat端
            var u = navigator.userAgent;
            if(u.toLowerCase().match(/MicroMessenger/i) == 'micromessenger') {
                // 略过loading
                if(sessionStorage.getItem('currentPage') != null || sessionStorage.getItem('currentPage') != undefined) {
                    $('.loading').hide();
                    app.router.goto(sessionStorage.getItem('currentPage'));
                    return;
                };
            };
            
            var context = this;

            // 动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.loading_page'), 0.2, { autoAlpha: 0, onStart: function() {
                // 预加载
                var firstLoadFiles = {
                    "files": [
                        // common
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/bg.jpg",
                            "size": 40960
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/bg-table.png",
                            "size": 347136
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/btn-back.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/btn-home.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/stars1224.png",
                            "size": 9216
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/stars1336.png",
                            "size": 10240
                        },
                        // home
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/title2.png",
                            "size": 81920
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/btn-check.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/btn-study.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/out.png",
                            "size": 5120
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/modal.png",
                            "size": 113664
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/y.png",
                            "size": 2048
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/n.png",
                            "size": 2048
                        },
                        // login
                        {
                            "type": "IMAGE",
                            "source": "assets/images/login/bg-form.png",
                            "size": 251904
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/login/stars.png",
                            "size": 10240
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/login/title.png",
                            "size": 9216
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/login/name.png",
                            "size": 5120
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/login/number.png",
                            "size": 5120
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/login/pwd.png",
                            "size": 5120
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/login/reg.png",
                            "size": 778
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/login/btn-con.png",
                            "size": 4096
                        },
                        // reg
                        {
                            "type": "IMAGE",
                            "source": "assets/images/reg/bg-form.png",
                            "size": 316416
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/reg/stars.png",
                            "size": 9216
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/reg/title.png",
                            "size": 12288
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/reg/city.png",
                            "size": 5120
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/reg/date.png",
                            "size": 5120
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/reg/pwdcon.png",
                            "size": 5120
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/reg/teacher.png",
                            "size": 5120
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/reg/btn-reg.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/reg/btn-login.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/reg/modal.png",
                            "size": 121856
                        },
                        // modules
                        {
                            "type": "IMAGE",
                            "source": "assets/images/modules/title.png",
                            "size": 10240
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/modules/module-study.png",
                            "size": 32768
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/modules/module-test.png",
                            "size": 32768
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/modules/module-prize.png",
                            "size": 32768
                        },
                        // program
                        {
                            "type": "IMAGE",
                            "source": "assets/images/program/bg-program.png",
                            "size": 198656
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/program/title.png",
                            "size": 9216
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/program/level1.png",
                            "size": 777
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/program/level2.png",
                            "size": 991
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/program/level3.png",
                            "size": 1024
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/program/nopass.png",
                            "size": 6144
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/program/pass.png",
                            "size": 5120
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/program/btn-study.png",
                            "size": 3072
                        },
                        // guide
                        {
                            "type": "IMAGE",
                            "source": "assets/images/guide/title1.png",
                            "size": 16384
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/guide/title2.png",
                            "size": 16384
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/guide/title3.png",
                            "size": 17408
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/guide/guide1.png",
                            "size": 47104
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/guide/guide2.png",
                            "size": 28672
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/guide/guide3.png",
                            "size": 35840
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/guide/btn-test.png",
                            "size": 4096
                        },
                        // test
                        {
                            "type": "IMAGE",
                            "source": "assets/images/test/title.png",
                            "size": 10240
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/test/title1.png",
                            "size": 18432
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/test/title2.png",
                            "size": 18432
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/test/title3.png",
                            "size": 18432
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/test/btn10.png",
                            "size": 21504
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/test/btn11.png",
                            "size": 30720
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/test/btn20.png",
                            "size": 21504
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/test/btn21.png",
                            "size": 30720
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/test/btn30.png",
                            "size": 22528
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/test/btn31.png",
                            "size": 28672
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/test/btn-submit.png",
                            "size": 4096
                        },
                        // analyse
                        {
                            "type": "IMAGE",
                            "source": "assets/images/analyse/title1.png",
                            "size": 12288
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/analyse/score.png",
                            "size": 16384
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/analyse/board.png",
                            "size": 22528
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/analyse/btn-pass0.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/analyse/btn-pass1.png",
                            "size": 3072
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/analyse/btn-retest0.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/analyse/btn-retest1.png",
                            "size": 4096
                        },
                        // prize
                        {
                            "type": "IMAGE",
                            "source": "assets/images/prize/bg-prize.png",
                            "size": 188416
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/prize/title.png",
                            "size": 10240
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/prize/apply.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/prize/exchange.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/prize/success-apply.png",
                            "size": 7168
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/prize/success-exchange.png",
                            "size": 7168
                        },
                        // check
                        {
                            "type": "IMAGE",
                            "source": "assets/images/check/stars.png",
                            "size": 11264
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/check/title.png",
                            "size": 8192
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/check/shop.png",
                            "size": 17408
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/check/area.png",
                            "size": 17408
                        },
                        // search
                        {
                            "type": "IMAGE",
                            "source": "assets/images/search/stars.png",
                            "size": 11264
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/search/title-shop.png",
                            "size": 14336
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/search/title-area.png",
                            "size": 13312
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/search/bg-table.png",
                            "size": 74752
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/search/input.png",
                            "size": 30720
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/search/search.png",
                            "size": 8192
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/search/btn-check.png",
                            "size": 4096
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/search/btn-all.png",
                            "size": 4096
                        },
                        // list
                        {
                            "type": "IMAGE",
                            "source": "assets/images/list/bg-shop.png",
                            "size": 244736
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/list/bg-area.png",
                            "size": 291840
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/list/title-shop.png",
                            "size": 11264
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/list/title-area.png",
                            "size": 12288
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/list/title-all.png",
                            "size": 11264
                        },
                    ]
                };
                $.html5Loader({
                    filesToLoad: firstLoadFiles,
                    onBeforeLoad: function() {},
                    onElementLoaded: function(obj, elm) {},
                    onUpdate: function(percentage) {
                        // console.log(percentage);
                        $('.num').html(percentage);
                    },
                    onComplete: function() {
                        setTimeout(function() {
                            tl.kill();
                            app.router.goto('home');
                        }, 1000);
                    }
                });
            }, onComplete: function() {
                //
            }});  
        }
    });
    //  Return the module for AMD compliance.
    return Page;
});
