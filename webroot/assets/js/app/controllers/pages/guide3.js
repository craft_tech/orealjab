// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require([],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'guide3'); 
            };

            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.guide3_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // test
            $('.btn-test').on('click', function() {
                if(JSON.parse(localStorage.getItem('level3score')) < 100) {
                    tl.kill();
                    app.router.goto('test3', ['guide3']);
                } else {
                    alert('您已通过测试');
                }
            });

            // back program
            $('.btn-back').on('click', function() {
                tl.kill();
                if(sessionStorage.getItem('fromAnalyse3') != null || sessionStorage.getItem('fromAnalyse3') != undefined) {
                    sessionStorage.removeItem('fromAnalyse3');
                    app.router.goto('modules');
                } else {
                    app.router.goto('program');
                };
            });
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
