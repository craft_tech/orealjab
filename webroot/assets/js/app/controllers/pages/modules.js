// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require([],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'modules');
            };

            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.modules_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                // 
            } }, 0.1);

            // study
            $('.study').on('click', function() {
                    tl.kill();
                    app.router.goto('program');
            });

            // test
            $('.test').on('click', function() {
                tl.kill();
                app.router.goto('test');
            });

            // prize
            $('.prize').on('click', function() {
                    tl.kill();
                    app.router.goto('prize');
            });

            // back home
            $('.btn-home').on('click', function() {
                    tl.kill();
                    app.router.goto('home');
            });
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
