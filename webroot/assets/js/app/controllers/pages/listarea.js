// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/fx", "vendor/zepto/fx_methods"],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'listarea'); 
            };

            // 获取最新区域进度
            $('.modal-loading').fadeIn('fast');
            $.ajax({
                type: 'post',
                url: 'https://campaign.lorealparis.com.cn/jablearning/Server/GetRegionalProgress',
                dataType: 'json',
                // contentType: 'application/json',
                data: {
                    regional: sessionStorage.getItem('teacName')
                },
                // async: false,
                cache: false,
                success: function(data) {
                    console.log(data);
                    // if(data.msg != '') {
                    //     alert(data.msg);
                    // };
                    if(data.status) {
                        var len = data.data.length;
                        if(len > 0) {
                            // 有数据
                            for(var i = 0; i < len; i++) {
                                var status = '--';
                                switch(data.data[i].StudyStatus) {
                                    case 0:
                                        break;
                                    case 1:
                                        status = '在读';
                                        break;
                                    case 2:
                                        status = '复读';
                                        break;
                                    case 3:
                                        status = '毕业';
                                        break;
                                    default:
                                        //   
                                }; 
                                var $div = $('<div class="list-item"><div class="teac text-center">' + data.data[i].Trainer + '</div><div class="area">' + data.data[i].City + '</div><div class="num text-center">' + data.data[i].StoreNum + '</div><div class="name text-center">' + data.data[i].Name + '</div><div class="list-score"><div class="wrapper-score rel"><div class="score-item score-level1">种子期<span>' + data.data[i].EntryLevelScore + '</span><i class="icon-arrow"></i></div><div class="score-more abs full-w"><div class="score-item score-level2">成长期<span>' + data.data[i].AdvancedScore.toFixed(2) + '</span></div><div class="score-item score-level3">成熟期<span>' + data.data[i].MatureScore + '</span></div></div></div></div><div class="list-percent"><div class="wrapper-percent rel"><div class="percent-item percent1">兰&nbsp;&nbsp;&nbsp;蔻<span>' + data.data[i].LancomeRatio + '%</span><i class="icon-arrow"></i></div><div class="percent-more abs full-w"><div class="percent-item percent2">植村秀<span>' + data.data[i].ShuUemuraRatio + '%</span></div><div class="percent-item percent3">碧欧泉<span>' + data.data[i].BiothermRatio + '%</span></div><div class="percent-item percent4">阿玛尼<span>' + data.data[i].AmaniRatio + '%</span></div></div></div></div><div class="status text-center">' + status + '</div></div>');
                                $('.table-list').append($div);
                            };
                        } else {
                            $('.nodata').fadeIn('fast');
                        };
                    }
                },
                error: function() {
                    //
                    alert('获取进度失败，请刷新重试');
                },
                complete: function(data) {
                    $('.modal-loading').fadeOut('fast');
                    // score
                    $('.score-level1').on('click', function() {
                        $(this).next().toggleClass('show');
                    });

                    // percent
                    $('.percent1').on('click', function() {
                        $(this).next().toggleClass('show');
                    });
                }
            });

            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.listarea_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // back check
            $('.btn-check').on('click', function() {
                tl.kill();
                app.router.goto('check');
            });
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
