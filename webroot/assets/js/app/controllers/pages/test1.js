// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require(["vendor/zepto/fx", "vendor/zepto/fx_methods"],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'test1'); 
            };

            if(JSON.parse(localStorage.getItem('level1score')) == 0) {
                // 要答所有题目
                $('.qa-item').addClass('show');
            } else {
                // 调取错过的题目
                $('.modal-loading').fadeIn('fast');
                $.ajax({
                    type: 'post',
                    url: 'https://campaign.lorealparis.com.cn/jablearning/server/GetWrongQusetions',
                    dataType: 'json',
                    // contentType: 'application/json',
                    data: {
                        name: localStorage.getItem('userName'),
                        storeNum: localStorage.getItem('userNum'),
                        level: 1
                    },
                    // async: false,
                    cache: false,
                    success: function(data) {
                        console.log(data);
                        if(data.msg != '') {
                            if(/未登录+/.test(data.msg)) {
                                localStorage.removeItem('userName');
                                localStorage.removeItem('userNum');
                                tl.kill();
                                app.router.goto('login');
                                return;
                            };
                        };
                        if(data.status) {
                            var len = data.WrongQuestion.length;
                            if(len > 0) {
                                for(var i = 0; i < len; i++) {
                                    $('.qa-item' + (data.WrongQuestion.split(','))[i]).addClass('show');
                                };
                            };
                        };
                    },
                    error: function() {
                        //
                        alert('网络错误，请刷新重试');
                    },
                    complete: function(data) {
                        // 调取完毕
                        $('.modal-loading').fadeOut('fast');
                    }
                });
            };
            
            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.test1_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // 选择题
            function quesBox(el, num) {
                el.on('click', function() {
                    if($(this).prop('checked')) {
                        // checked
                        $('.q' + num + ' .' + $(this).attr('name').charAt(0)).removeClass('hide');
                    } else {
                        // cancel checked
                        $('.q' + num + ' .' + $(this).attr('name').charAt(0)).addClass('hide');
                    };
                });
            };
            // q1
            quesBox($('.a1 input'), 1);
            // q2
            quesBox($('.a2 input'), 2);
            // q3
            quesBox($('.a3 input'), 3);
            // q6
            quesBox($('.a6 input'), 6);
            // q8
            quesBox($('.a8 input'), 8);
            // q10
            quesBox($('.a10 input'), 10);
            // q11
            quesBox($('.a11 input'), 11);
            // q13
            quesBox($('.a13 input'), 13);
            // q14
            quesBox($('.a14 input'), 14);
            // q15
            quesBox($('.a15 input'), 15);

            // 禁止粘贴
            $('.q input').on('paste', function() {
                return false;
            });

            // 验证输入
            // q4
            $('.q4 input').on('input propertychange', function() {
                if(/^(A|B|C|D|E)+$/ig.test($(this).val())) {
                    // 
                } else {
                    $(this).val('');
                }
            });

            //q5
            $('.q5 input').on('input propertychange', function() {
                if(/^(A|B|C|D)+$/ig.test($(this).val())) {
                    // 
                } else {
                    $(this).val('');
                }
            });

            //q7
            $('.q7 input').on('input propertychange', function() {
                if(/^(A|B|C|D)+$/ig.test($(this).val())) {
                    // 
                } else {
                    $(this).val('');
                }
            });

            //q9
            $('.q9 input').on('input propertychange', function() {
                if(/^(A|B|C|D|E|F)+$/ig.test($(this).val())) {
                    // 
                } else {
                    $(this).val('');
                }
            });

            //q12
            $('.q12 input').on('input propertychange', function() {
                if(/^(A|B|C|D)+$/ig.test($(this).val())) {
                    // 
                } else {
                    $(this).val('');
                }
            });

            // 验证是否完成
            function checkA(el, num, type) {
                if(el.parents('.qa-item').hasClass('show')) {
                    if(type == 'box') {
                        // 选择
                        if(el.find('input:checked').length == 0) {
                            alert('请完成第' + num + '题');
                            return false;
                        } else {
                            return true;
                        };
                    } else {
                        // 填空
                        for(var i = 0; i < el.length; i++) {
                            if(el[i].value == '') {
                                alert('请完成第' + num + '题');
                                return false;
                            };
                        };
                        return true;
                    };
                } else {
                    // skip
                    return true;
                };
            };

            // 输出答题数组
            function AnsArr(el, type) {
                var arr = [];
                if(type == 'box') {
                    // 选择
                    for(var i = 0; i < el.length; i++) {
                        arr.push($(el[i]).attr('name').charAt(0));
                    };
                } else {
                    // 填空
                    for(var i = 0; i < el.length; i++) {
                        arr.push($(el[i]).val().toUpperCase());
                    };
                };
                return arr;
            };

            // submit
            var canSubmit = 1; // 默认可以提交
            $('.test1_page .btn-submit').off('click');
            $('.test1_page .btn-submit').on('click', function() {
                if(checkA($('.a1'), 1, 'box') && checkA($('.a2'), 2, 'box') && checkA($('.a3'), 3, 'box') && checkA($('.q4 input'), 4, 'input') && checkA($('.q5 input'), 5, 'input') && checkA($('.a6'), 6, 'box') && checkA($('.q7 input'), 7, 'input') && checkA($('.a8'), 8, 'box') && checkA($('.q9 input'), 9, 'input') && checkA($('.a10'), 10, 'box') && checkA($('.a11'), 11, 'box') && checkA($('.q12 input'), 12, 'input') && checkA($('.a13'), 13, 'box') && checkA($('.a14'), 14, 'box') && checkA($('.a15'), 15, 'box') ) {
                    var len = $('.qa-item').length,
                        currentLen = $('.qa-item.show').length,
                        answerO = {}, // 需要提交的答案
                        anAll = [];   // 所有答案
                    for(var i = 0; i < len; i++) {
                        if($($('.qa-item')[i]).hasClass('show')) {
                            // 显示的题目
                            if($($('.qa-item')[i]).data('type') == 'box') {
                                answerO['an' + $($('.qa-item')[i]).data('index')] = AnsArr($($('.qa-item')[i]).find('.a input:checked'), 'box').join(',');
                            } else {
                                answerO['an' + $($('.qa-item')[i]).data('index')] = AnsArr($($('.qa-item')[i]).find('.q input'), 'input').join(',');
                            };
                            anAll.push(answerO['an' + $($('.qa-item')[i]).data('index')]);
                        } else {
                            // 隐藏的题目
                            anAll.push('');
                        };
                    };
                    // console.log(JSON.stringify(answerO));
                    // console.log(anAll);
                    // can submit
                    if(canSubmit == 1) {
                        // 调用中不能再次提交
                        canSubmit = 0;
                        $('.modal-loading').fadeIn('fast');
                        $.ajax({
                            type: 'post',
                            url: 'https://campaign.lorealparis.com.cn/jablearning/Server/GetScore',
                            dataType: 'json',
                            // contentType: 'application/json',
                            data: {
                                name: localStorage.getItem('userName'),
                                storeNum: localStorage.getItem('userNum'),
                                level: 1,
                                answer: JSON.stringify(answerO)
                            },
                            // async: false,
                            cache: false,
                            success: function(data) {
                                console.log(data);
                                if(data.msg != '') {
                                    // alert(data.msg);
                                    if(/未登录+/.test(data.msg)) {
                                        localStorage.removeItem('userName');
                                        localStorage.removeItem('userNum');
                                        tl.kill();
                                        app.router.goto('login');
                                        return;
                                    };
                                };
                                if(data.status) {
                                    localStorage.setItem('studyStatus', JSON.stringify(data.data.StudyStatus)); // 存储学习状态
                                    localStorage.setItem('level1score', JSON.stringify(data.score)); // 更新分数
                                    sessionStorage.setItem('errorArr1', JSON.stringify(data.ean)); // 存储错题组
                                    if(data.ean.length > 0) {
                                        // 有错题
                                        sessionStorage.setItem('rightNum1', JSON.stringify(currentLen - data.ean.split(',').length)); // 存储对题数
                                        sessionStorage.setItem('wrongNum1', JSON.stringify(data.ean.split(',').length)); // 存储错题数
                                    } else {
                                        // 全对
                                        sessionStorage.setItem('rightNum1', JSON.stringify(currentLen)); // 存储对题数
                                        sessionStorage.setItem('wrongNum1', JSON.stringify(0)); // 存储错题数
                                    };
                                    sessionStorage.setItem('anAll', JSON.stringify(anAll)); // 存储答题选项
                                    tl.kill();
                                    app.router.goto('analyse1');
                                };
                            },
                            error: function() {
                                //
                                alert('提交失败，请重试');
                            },
                            complete: function(data) {
                                // 调取完毕
                                canSubmit = 1;
                                $('.modal-loading').fadeOut('fast');
                            }
                        });
                    };
                };
            });
            
            // back
            $('.btn-back').on('click', function() {
                tl.kill();
                app.router.goto(context.args[0]);
            });
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
