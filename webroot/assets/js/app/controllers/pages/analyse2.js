// Page module
define(["app", "controllers/base/page"],

function(app, BasePage) {
    var Page = {};

    Page.View = BasePage.View.extend({
        fitOn: "width", //width, height, custom
        beforeRender: function() {
            var done = this.async();
            require([],
            function() {
                done();
            });
        },
        afterRender: function() {
            // 不允许单独单开某个页面
            if(sessionStorage.getItem('loading') != null || sessionStorage.getItem('loading') != undefined) {
                sessionStorage.setItem('currentPage', 'analyse2'); 
            };

            // fill
            $('.score').text(JSON.parse(localStorage.getItem('level2score')).toFixed(2));
            $('.right-count').text(JSON.parse(sessionStorage.getItem('rightNum2')));
            $('.wrong-count').text(JSON.parse(sessionStorage.getItem('wrongNum2')));
            var wrongArr = JSON.parse(sessionStorage.getItem('errorArr2'));
            if(JSON.parse(sessionStorage.getItem('wrongNum2')) > 0) {
                // 有错题
                var len = wrongArr.length;
                for(var i = 0; i < len; i++) {
                    $('.qa-item' + wrongArr[i]).addClass('show');
                    if(wrongArr[i] <= 8) {
                        // 属于第一组
                        $('.qa-title1').addClass('show');
                    } else if(wrongArr[i] > 8 && wrongArr[i] <= 16) {
                        // 属于第二组
                        $('.qa-title2').addClass('show');
                    } else if(wrongArr[i] > 16 && wrongArr[i] <= 21) {
                        // 属于第三组
                        $('.qa-title3').addClass('show');
                    } else {
                        // 属于第四组
                        $('.qa-title4').addClass('show');
                    };
                };
            };

            // 根据分数判断按钮是否点亮
            if(JSON.parse(localStorage.getItem('level2score')) < 3.5) {
                // 没过
                $('.btn-retest').addClass('again');
                $('.btn-retest').attr('src', 'assets/images/analyse/btn-retest1.png');
                $('.btn-pass').attr('src', 'assets/images/analyse/btn-pass0.png');
            } else if(JSON.parse(localStorage.getItem('level2score')) >= 3.5 && JSON.parse(localStorage.getItem('level2score')) < 5) {
                // 过了不是满分
                $('.btn-retest').addClass('again');
                $('.btn-pass').addClass('pass');
                $('.btn-retest').attr('src', 'assets/images/analyse/btn-retest1.png');
                $('.btn-pass').attr('src', 'assets/images/analyse/btn-pass1.png');
            } else {
                // 满分
                $('.btn-pass').addClass('pass');
                $('.btn-retest').attr('src', 'assets/images/analyse/btn-retest0.png');
                $('.btn-pass').attr('src', 'assets/images/analyse/btn-pass1.png');
            };
             
            var context = this;

            //动画效果
            var tl = new TimelineMax();
            tl.from(context.$('.analyse2_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                //
            } }, 0.1);

            // retest
            $('.btn-retest.again').on('click', function() {
                tl.kill();
                sessionStorage.setItem('fromAnalyse2', true);
                app.router.goto('guide2');
            });

            // pass
            $('.btn-pass.pass').on('click', function() {
                tl.kill();
                app.router.goto('prize');
            });
        },
        resize: function(ww, wh) {
            //
        },
        afterRemove: function() {},
    })
    //  Return the module for AMD compliance.
    return Page;
})
